﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;

namespace test
{
    partial class NotificationService : ServiceBase
    {
        private Timer timer = new Timer();
        private double servicePollInterval;
        private int id;
        private int status;
        private string message;

        public NotificationService()
        {
            servicePollInterval = 2000;
            if (notifyIcon1 == null) {
                notifyIcon1 = new System.Windows.Forms.NotifyIcon();
            }
            notifyIcon1.BalloonTipText = "Application minimized";
            notifyIcon1.BalloonTipTitle = "dotprog";
            // When the program begins, show the balloon on the icon for one second.
            
        }

        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service. 
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            //providing the time in miliseconds 
            timer.Interval = servicePollInterval;
            timer.AutoReset = true;
            timer.Enabled = true;
            timer.Start();
        }

        protected override void OnContinue()
        {
            base.OnContinue();
            timer.Start();
        }

        protected override void OnPause()
        {
            base.OnPause();
            timer.Stop();
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service. 
            timer.Stop();
        }

        protected override void OnShutdown()
        {
            base.OnShutdown();
            timer.Stop();
        }

        void timer_Elapsed(object sender, EventArgs e)
        {
            notifyIcon1.ShowBalloonTip(1000);
        }
    }
}
