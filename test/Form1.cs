﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using System.Windows.Forms;

namespace test
{
    public partial class Form1 : Form
    {

        private static readonly string StartupKey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";
        private static readonly string StartupValue = "FirePointsShedullerService_";

        private static void SetStartup()
        {
            //Set the application to run at startup
            RegistryKey key = Registry.CurrentUser.OpenSubKey(StartupKey, true);
            var valueNames = key.GetValueNames().ToList();
            if (valueNames.IndexOf(StartupValue) < 0 || key.GetValue(StartupValue) == null || string.IsNullOrEmpty(key.GetValue(StartupValue).ToString())) {
                key.SetValue(StartupValue, Application.ExecutablePath.ToString());
            }       

        }

        public Form1()
        {
            InitializeComponent();
            SetStartup();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            notifyIcon1.BalloonTipText = "Найдена термальная точка!!!";
            notifyIcon1.BalloonTipTitle = "Термальные точки";
            // When the program begins, show the balloon on the icon for one second.
            notifyIcon1.ShowBalloonTip(5000);
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            // When icon is double-clicked, show this message.
            //MessageBox.Show("Doing something important on double-click...");
            ShowInTaskbar = true;
            // Then, hide the icon.
            notifyIcon1.Visible = false;
            WindowState = FormWindowState.Normal;
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized) {
                ShowIcon = false;
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(1000);
            }
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
    }
}
