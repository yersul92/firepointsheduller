﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FirePointsNotification
{
    public partial class FormConfirmRegion : Form
    {
        public FormConfirmRegion(Dictionary<string,string> cbxDataSource)
        {
            InitializeComponent();
            comboBox1.DataSource = new BindingSource(cbxDataSource, null);
            comboBox1.DisplayMember = "Value";
            comboBox1.ValueMember = "Key";
            this.ControlBox = false;
            comboBox1.Refresh();

        }

        
    }
}
