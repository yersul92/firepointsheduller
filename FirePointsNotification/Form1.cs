﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;


namespace FirePointsNotification
{
    public partial class Form1 : Form
    {
        #region flass form params
        [DllImport("user32.dll")]
        static extern Int32 FlashWindowEx(ref FLASHWINFO pwfi);
        [StructLayout(LayoutKind.Sequential)]
        public struct FLASHWINFO
        {
            public UInt32 cbSize;
            public IntPtr hwnd;
            public Int32 dwFlags;
            public UInt32 uCount;
            public Int32 dwTimeout;
        }
        // stop flashing
        const int FLASHW_STOP = 0;

        // flash the window title
        const int FLASHW_CAPTION = 1;

        // flash the taskbar button
        const int FLASHW_TRAY = 2;

        // 1 | 2
        const int FLASHW_ALL = 3;

        // flash continuously
        const int FLASHW_TIMER = 4;

        // flash until the window comes to the foreground
        const int FLASHW_TIMERNOFG = 12;
        #endregion

        #region auto start params

        private static readonly string StartupKey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";
        private static readonly string StartupValue = "FirePointsShedullerService_";
        private static readonly string alarmSoundPath = Path.GetDirectoryName(Application.ExecutablePath) + "\\sound.wav";
        private static void SetStartup()
        {
            //Set the application to run at startup
            RegistryKey key = Registry.CurrentUser.OpenSubKey(StartupKey, true);
            var valueNames = key.GetValueNames().ToList();
            if (valueNames.IndexOf(StartupValue) < 0 || key.GetValue(StartupValue) == null || string.IsNullOrEmpty(key.GetValue(StartupValue).ToString()))
            {
                key.SetValue(StartupValue, Application.ExecutablePath.ToString());
            }

        }

        #endregion

        private const string regionKey = "region_id";
        private List<string> foundIds = new List<string>();
        public Form1()
        {
            InitializeComponent();
            SetStartup();
            Shown += Form1_Shown;
        }
        System.Windows.Forms.Timer _timer;
        private void setRowNumber(DataGridView dgv)
        {
            foreach (DataGridViewRow row in dgv.Rows)
            {
                row.HeaderCell.Value = (row.Index + 1).ToString();
            }
        }
        #region regions list
        private Dictionary<string, string> _regions = new Dictionary<string, string> {
            {"0","Все" },
            { "1","Акмолинская область" },
            {"2","Актюбинская область" },
            {"3","Алматинская область"},
            {"4","Атырауская область"},
            {"5","Восточно-Казахстанская область"},
            {"6","Жамбылская область" },
            {"7","Западно-Казахстанская область"},
            {"8","Карагандинская область"},
            {"9","Костанайская область"},
            {"10","Кызылординская область"},
            {"11","Мангыстауская область"},
            {"12","Павлодарская область"},
            {"13","Северо-Казахстанская область"},
            {"14","Южно-Казахстанская область"}};
        #endregion
        #region dsitrict list
        private Dictionary<int, string> _districts = new Dictionary<int, string>{
            {177,"Аулиекольский район"},
            {178,"территория городского акимата г. Лисаковск"},
            {179,"территория городского акимата г. Рудный"},
            {180,"Житыкаринский район"},
            {181,"Жанкельдынский район"},
            {182,"территория городского акимата г. Аркалык"},
            {183,"Сарыкольский район"},
            {184,"Карабалыкский район"},
            {185,"Костанайский район"},
            {186,"Камыстынский район"},
            {187,"Аманкельдынский район"},
            {188,"Карасуский район"},
            {189,"Узынкольский район"},
            {190,"территория городского акимата г. Костанай"},
            {191,"Алтынсаринский район"},
            {192,"Наурызымский район"},
            {193,"Денисовский район"},
            {194,"Мендыкаринский район"},
            {195,"Тарановский район"},
            {196,"Федоровский район"},
            {197,"Жанакорганский район"},
            {210,"Успенский район"},
            {198,"Казалынский район"},
            {199,"Егиндыкольский район"},
            {200,"Кармакшынский район"},
            {201,"Сырдариинский район"},
            {202,"Зерендинский район"},
            {206,"Иртышский район"},
            {203,"Аральский район"},
            {219,"Есильский район"},
            {220,"Енбекшиказахский район"},
            {204,"территория городского акимата г. Актау"},
            {205,"Екибастузский район"},
            {207,"Качирский район"},
            {208,"Майский район"},
            {209,"Павлодарский район"},
            {211,"Лебяжинский район"},
            {212,"Щарбактинский район"},
            {287,"Жаксынский район"},
            {213,"Актогайский район"},
            {214,"Хобдинский район"},
            {228,"Тайыншинский район"},
            {215,"Акжарский район"},
            {216,"Алакольский район"},
            {217,"территория городского акимата г. Алматы"},
            {218,"район Магжана Жумабаева"},
            {221,"Кызылжарский район"},
            {222,"Мамлютский район"},
            {223,"Жамбылский район"},
            {224,"Илийский район"},
            {225,"Капшагайский район"},
            {226,"Карасайский район"},
            {227,"Каратальский район"},
            {229,"Уалихановский район"},
            {230,"Айыртауский район"},
            {231,"район Габита Мусрепова"},
            {232,"Тимирязевский район"},
            {233,"Жамбылский район"},
            {234,"Саркандский район"},
            {235,"Талгарский район"},
            {236,"Аягозский район"},
            {237,"Катонrарагайский район"},
            {238,"Куршимский район"},
            {239,"территория городского акимата г. Туркистан"},
            {240,"Жуалынский район"},
            {241,"Кордайский район"},
            {242,"Мактааралский район"},
            {243,"район Байдибека"},
            {244,"Сайрамский район"},
            {245,"район Толе Би"},
            {246,"Тулькибасский район"},
            {247,"территория городского акимата г. Шымкент"},
            {248,"Отырарский район"},
            {249,"Арысский район"},
            {250,"Бокейординский район"},
            {251,"Борилинский район"},
            {252,"Казыгуртский район"},
            {253,"Жанибекский район"},
            {254,"Зеленовский район"},
            {255,"Казталовский район"},
            {262,"Жалагашский район"},
            {256,"Сарыагашский район"},
            {267,"Аккайынский район"},
            {257,"Шардаринский район"},
            {258,"Теректинский район"},
            {268,"Шал-Акынский район"},
            {259,"территория городского акимата г. Уральск"},
            {260,"Актогайский район"},
            {261,"Жанааркинский район"},
            {263,"территория городского акимата г. Кызылорда"},
            {264,"Шиелинский район"},
            {269,"Ордабасынский район"},
            {265,"Баянаульский район"},
            {266,"Железинский район"},
            {284,"территория городского акимата г. Семей"},
            {285,"Улытауский район"},
            {286,"Нуринский район"},
            {270,"Созакский район"},
            {271,"Макатский район"},
            {272,"территория городского акимата г. Атырау"},
            {273,"Бейнеуский район"},
            {274,"Мангыстауский район"},
            {275,"Каракиянский район"},
            {276,"Жылыойский район"},
            {277,"Исатайский район"},
            {278,"Тупкараганский район"},
            {279,"Жаркаинский район"},
            {280,"Есильский район"},
            {281,"Айтекебийский район"},
            {282,"Иргизский район"},
            {283,"Курмангазинский район"},
            {288,"Аршалынский район"},
            {289,"Астраханский район"},
            {290,"Мартукский район"},
            {291,"Атбасарский район"},
            {292,"Буландинский район"},
            {293,"район Биржан-Сал"},
            {294,"Ерейментауский район"},
            {295,"Коргалжынский район"},
            {296,"Сандыктауский район"},
            {297,"Целиноградский район"},
            {298,"Шортандинский район"},
            {299,"Щучинский район"},
            {300,"Аккольский район"},
            {301,"Алгинский район"},
            {302,"Байганинский район"},
            {303,"Каргалинский район"},
            {304,"Мугалжарский район"},
            {305,"Темирский район"},
            {306,"Уилский район"},
            {307,"Хромтауский район"},
            {308,"Коксуский район"},
            {309,"Шалкарский район"},
            {310,"Уйгурский район"},
            {311,"Балхашский район"},
            {312,"Ескельдинский район"},
            {313,"Кербулакский район"},
            {314,"Панфиловский район"},
            {315,"Райымбекский район"},
            {317,"Индерский район"},
            {316,"Аксуйский район"},
            {318,"территория городского акимата г. Риддер"},
            {319,"Кзылкогинский район"},
            {320,"Сарысуский район"},
            {329,"Бескарагайский район"},
            {321,"Махамбетский район"},
            {322,"Жарминский район"},
            {323,"Зайсанский район"},
            {324,"Зыряновский район"},
            {325,"Кокпектынский район"},
            {326,"Тарбагатайский район"},
            {327,"Уланский район"},
            {328,"Уржарский район"},
            {330,"Бородулихинский район"},
            {351,"Шетский район"},
            {331,"Глубоковский район"},
            {332,"Шемонаихинский район"},
            {333,"Байзакский район"},
            {334,"Абайский район"},
            {352,"Абайский район"},
            {335,"Меркенский район"},
            {336,"Мойынкумский район"},
            {337,"Таласский район"},
            {338,"Жамбылский район"},
            {339,"Турара Рыскулова район"},
            {340,"Шуский район"},
            {341,"Жанакалинский район"},
            {342,"Каратобинский район"},
            {343,"Сырымский район"},
            {344,"Таскалинский район"},
            {345,"Шынгырлауский район"},
            {346,"Акжайыкский район"},
            {347,"территория городского акимата г. Караганда"},
            {348,"Бухар Жырауский район"},
            {349,"Каркаралынский район"},
            {350,"Осакаровский район"}};
        #endregion

        private void Form1_Shown(object sender, EventArgs e)
        {
            this.Refresh();
            //var isFirstLoad = getSetting("is_first_load");
            //if (string.IsNullOrEmpty(isFirstLoad)) {

            //}
            // When the program begins, show the balloon on the icon for 5 second.
            //notifyIcon1.ShowBalloonTip(5000);
            WindowState = FormWindowState.Minimized;
            Timer1_Tick(null,null);


        }

        private void Timer1_Tick(object Sender, EventArgs e)
        {
            // Set the caption to the current time.
            var regionValue = getSetting(regionKey);
            if (!string.IsNullOrEmpty(regionValue)) {
                using (var client = new WebClient()) {
                    string cql_filter = string.Equals(regionValue, "0") ? "" : "&cql_filter=region_id=" + regionValue;
                    var result = client.DownloadString("http://portal.gharysh.kz:8080/geoserver/cite/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=cite:modis_c6_global_24h&maxFeatures=500&outputFormat=application%2Fjson"+ cql_filter);
                    if (!string.IsNullOrEmpty(result)) {
                        var resultObject = Newtonsoft.Json.Linq.JObject.Parse(result);
                        var featCount = Convert.ToInt32(resultObject["totalFeatures"]);
                        if (featCount > 0) {
                            Newtonsoft.Json.Linq.JArray features = Newtonsoft.Json.Linq.JArray.Parse(resultObject["features"].ToString());
                            var rows = new List<GridRow>();
                            var newPointFound = false;
                            for (int i = 0; i < features.Count; i++) {
                                var dateTimeString = string.Format("{0} {1}",features[i]["properties"]["acq_date"].ToString().Replace("Z",""), features[i]["properties"]["acq_time"]);
                                var parsedDate = DateTime.ParseExact(dateTimeString, "yyyy-MM-dd HH:mm:ss", null).AddHours(6).AddDays(1);
                                var dateNow = DateTime.Now;
                                var date_ = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, 0, 0, 0);
                                if (parsedDate < date_) {
                                    continue;
                                }
                                var districtId = Convert.ToInt32(features[i]["properties"]["district_id"]);
                                var regionId = features[i]["properties"]["region_id"];
                                string districtName = "";
                                _districts.TryGetValue(districtId, out districtName);
                                string regionName = "";
                                if (regionId != null)
                                {
                                    _regions.TryGetValue(regionId.ToString(), out regionName);
                                }

                                var gid = features[i]["id"].ToString().Replace("modis_c6_global_24h.", "");
                                rows.Add(new GridRow {
                                    Date = parsedDate,
                                    Gid = gid,
                                    Location = districtName,
                                    Region = regionName
                                });
                                if (foundIds.Count==0||foundIds.IndexOf(gid) < 0)
                                {
                                    foundIds.Add(gid);
                                    newPointFound = true;
                                }
                            }
                            if (newPointFound) {
                                Flash(true);
                                notifyIcon1.ShowBalloonTip(5000);
                                WindowState = FormWindowState.Normal;
                                if (File.Exists(alarmSoundPath))
                                {
                                    new System.Media.SoundPlayer(alarmSoundPath).Play();
                                }
                            }
                            rows = rows.OrderByDescending(x => x.Date).ToList();
                            dgvFirePoints.Rows.Clear();
                            dgvFirePoints.Refresh();
                            for (int i = 0; i < rows.Count; i++)
                            {
                                var index = dgvFirePoints.Rows.Add();
                                dgvFirePoints.Rows[index].Cells["Id"].Value = rows[i].Gid;
                                dgvFirePoints.Rows[index].Cells["Time"].Value = rows[i].Date.ToString("yyyy-MM-dd HH:mm");
                                dgvFirePoints.Rows[index].Cells["Location"].Value = rows[i].Location;
                                dgvFirePoints.Rows[index].Cells["region"].Value = rows[i].Region;
                            }
                            //setRowNumber(dgvFirePoints);
                            dgvFirePoints.Refresh();
                        }

                    }
                }
            }
        }

        private static void setSetting(string key, string value)
        {
            Properties.Settings.Default[key] = value;
            Properties.Settings.Default.Save();
        }

        private static string getSetting(string key)
        {
           return Properties.Settings.Default[key].ToString();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _timer = new System.Windows.Forms.Timer();
            _timer.Interval = 1000*60*3;
            _timer.Tick += new EventHandler(this.Timer1_Tick);
            var regionValue = getSetting(regionKey);
            if (string.IsNullOrEmpty(regionValue))
            {
                using (FormConfirmRegion dialog = new FormConfirmRegion(_regions))
                {
                    DialogResult result = dialog.ShowDialog();
                    if (result == DialogResult.OK) {
                        setSetting(regionKey, dialog.comboBox1.SelectedValue.ToString());
                        _timer.Enabled = true;
                    }
                }
            }
            else
            {
                _timer.Enabled = true;
            }
            updateLblRegionText();

            notifyIcon1.BalloonTipText = "Найдена термальная точка!!!";
            notifyIcon1.BalloonTipTitle = "Термальные точки";
            
        }

        private void btnChangeRegion_Click(object sender, EventArgs e)
        {
            using (FormConfirmRegion dialog = new FormConfirmRegion(_regions))
            {
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    setSetting(regionKey, dialog.comboBox1.SelectedValue.ToString());
                }
            }
            updateLblRegionText();
        }

        private void updateLblRegionText() {
            var regionValue = getSetting(regionKey);
            string regionText = null;
            _regions.TryGetValue(regionValue, out regionText);
            lblRegionName.Text = string.IsNullOrEmpty(regionText) ? "Не указан" : regionText;
        }

        public class GridRow {
            public string Gid { get; set; }
            public DateTime Date { get; set; }
            public string Location { get; set; }
            public string Region { get; set; }
        }

        private void Flash(bool stop)
        {
            FLASHWINFO fw = new FLASHWINFO();

            fw.cbSize = Convert.ToUInt32(Marshal.SizeOf(typeof(FLASHWINFO)));
            fw.hwnd = Handle;
            if (!stop)
            {
                fw.dwFlags = 2;
            }
            else
                fw.dwFlags = 0;
            fw.uCount = UInt32.MaxValue;

            FlashWindowEx(ref fw);
        }


        private void Form1_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                //Flash(false);
                ShowIcon = false;
                notifyIcon1.Visible = true;
                //notifyIcon1.ShowBalloonTip(1000);
            }
            else
                Flash(true);
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //MessageBox.Show("Doing something important on double-click...");
            ShowInTaskbar = true;
            // Then, hide the icon.
            notifyIcon1.Visible = false;
            WindowState = FormWindowState.Normal;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _timer.Stop();
            _timer.Enabled = false;
            this.Close();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                const int CP_NOCLOSE = 0x200;
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE;
                return myCp;
            }
        }

        private void dgvFirePoints_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var grid = sender as DataGridView;
            var rowIdx = (e.RowIndex + 1).ToString();

            var centerFormat = new StringFormat()
            {
                // right alignment might actually make more sense for numbers
                Alignment = StringAlignment.Center,

                LineAlignment = StringAlignment.Center
            };
            //get the size of the string
            Size textSize = TextRenderer.MeasureText(rowIdx, this.Font);
            //if header width lower then string width then resize
            if (grid.RowHeadersWidth < textSize.Width + 40)
            {
                grid.RowHeadersWidth = textSize.Width + 40;
            }
            var headerBounds = new Rectangle(e.RowBounds.Left, e.RowBounds.Top, grid.RowHeadersWidth, e.RowBounds.Height);
            e.Graphics.DrawString(rowIdx, this.Font, SystemBrushes.ControlText, headerBounds, centerFormat);
        }
    }
}
