﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Net;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Diagnostics;
using Catfood.Shapefile;
using Npgsql;
using System.Threading;

namespace FirePointsSheduller
{
    public partial class Form1 : Form
    {
        private string downloadPathZip = System.Windows.Forms.Application.StartupPath + "\\temp_.zip";
        private string extractPath = System.Windows.Forms.Application.StartupPath + "\\temp_";
        private string downloadPathShp = System.Windows.Forms.Application.StartupPath + "\\temp_\\MODIS_C6_Global_24h.shp";
        
        public Form1()
        {
            InitializeComponent();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            _thread.Abort();
            btnStart.Enabled = true;
            btnStop.Enabled = false;
            logInfo(string.Format("Загрузка данных принудительно остановлена в {0}",getCurrentTime()));
        }
        Thread _thread;
        private void btnStart_Click(object sender, EventArgs e)
        {
            _thread = new Thread(new ThreadStart(proccess));
            _thread.Start();
            btnStop.Enabled = true;
            btnStart.Enabled = false;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void proccess()
        {
            try
            {
                //var logInfo = new Action<string>((string message) => { listBoxStatusess.Items.Insert(0, message); });
                while (true)
                {
                    var features = new List<Feature>();
                    #region Download shapefile from net
                    using (WebClient wc = new WebClient())
                    {

                        logInfo("Началась загрузка данных из интернета...");

                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | (SecurityProtocolType)768 | (SecurityProtocolType)3072;
                        byte[] myDataBuffer = wc.DownloadData((new Uri("https://firms.modaps.eosdis.nasa.gov/active_fire/c6/shapes/zips/MODIS_C6_Global_24h.zip")));

                        MemoryStream storeStream = new MemoryStream();

                        storeStream.SetLength(myDataBuffer.Length);
                        storeStream.Write(myDataBuffer, 0, (int)storeStream.Length);

                        storeStream.Flush();

                        //TO save into certain file must exist on Local
                        using (FileStream outStream = File.OpenWrite(downloadPathZip))
                        {
                            storeStream.WriteTo(outStream);
                            outStream.Flush();
                            outStream.Close();
                        }
                        logInfo("Загрузка данных из завершилась...");
                    }
                    #endregion
                    #region extracting shapefile from an archive
                    logInfo("Началась распаковка данных");
                    using (ZipInputStream s = new ZipInputStream(File.OpenRead(downloadPathZip)))
                    {

                        ZipEntry theEntry;
                        Directory.CreateDirectory(extractPath);
                        while ((theEntry = s.GetNextEntry()) != null)
                        {
                            if (!string.IsNullOrEmpty(theEntry.Name))
                            {
                                using (FileStream streamWriter = File.Create(extractPath + "\\" + theEntry.Name))
                                {
                                    int size = 2048;
                                    byte[] data = new byte[2048];
                                    while (true)
                                    {
                                        size = s.Read(data, 0, data.Length);
                                        if (size > 0)
                                        {
                                            streamWriter.Write(data, 0, size);
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    logInfo("Распаковка данных завершилась");
                    #endregion
                    #region Read Data from shapefile
                    logInfo("Начался чтение данных из Shapefile...");
                    using (Shapefile shapefile = new Shapefile(downloadPathShp))
                    {
                        logInfo(string.Format("ShapefileDemo Dumping {0}", downloadPathShp));


                        // a shapefile contains one type of shape (and possibly null shapes)
                        logInfo(string.Format("Type: {0}, Shapes: {1:n0}", shapefile.Type, shapefile.Count));

                        // a shapefile also defines a bounding box for all shapes in the file
                        logInfo(string.Format("Bounds: {0},{1} -> {2},{3}",
                            shapefile.BoundingBox.Left,
                            shapefile.BoundingBox.Top,
                            shapefile.BoundingBox.Right,
                            shapefile.BoundingBox.Bottom));

                        // enumerate all shapes
                        var currentHour = DateTime.Now.Hour;
                        foreach (Shape shape in shapefile)
                        {
                            var date = new DateTime();
                            if (!DateTime.TryParse(shape.GetMetadata("acq_date"), out date))
                            {
                                continue;
                            }
                            int dayToAdd = 0;
                            int hoursToAdd = 0;
                            var minute = Convert.ToInt32(shape.GetMetadata("acq_time").Substring(2, 2));
                            if (minute >= 60)
                            {
                                minute = minute - 60;
                                hoursToAdd = 1;
                            }
                            var hour = Convert.ToInt32(shape.GetMetadata("acq_time").Substring(0, 2)) + hoursToAdd;
                            if (hour >= 24)
                            {
                                hour = hour - 24;
                                dayToAdd = 1;
                            }

                            var parsedDate = new DateTime(date.Year, date.Month, date.Day, hour, minute, 0);
                            if (dayToAdd > 0)
                            {
                                parsedDate.AddDays(dayToAdd);
                            }
                            var now = DateTime.Now;

                            if (now.Hour < 8)
                            {

                            }
                            else
                            {
                                if (now.Date == parsedDate.Date && parsedDate.Hour > 7)
                                {

                                }
                                else
                                {
                                    continue;
                                }
                            }
                            var f = new Feature();
                            f.brightness = !string.IsNullOrEmpty(shape.GetMetadata("brightness")) ? float.Parse(shape.GetMetadata("brightness"),
  System.Globalization.CultureInfo.InvariantCulture) : 0;
                            f.scan = !string.IsNullOrEmpty(shape.GetMetadata("scan")) ? float.Parse(shape.GetMetadata("scan"),
  System.Globalization.CultureInfo.InvariantCulture) : 0;
                            f.track = !string.IsNullOrEmpty(shape.GetMetadata("track")) ? float.Parse(shape.GetMetadata("track"),
  System.Globalization.CultureInfo.InvariantCulture) : 0;
                            f.acq_date = shape.GetMetadata("acq_date");
                            f.acq_time = shape.GetMetadata("acq_time");
                            f.satellite = shape.GetMetadata("satellite");
                            f.confidence = !string.IsNullOrEmpty(shape.GetMetadata("confidence")) ? float.Parse(shape.GetMetadata("confidence"),
  System.Globalization.CultureInfo.InvariantCulture) : 0;
                            f.version = shape.GetMetadata("version");
                            f.bright_t31 = !string.IsNullOrEmpty(shape.GetMetadata("bright_t31")) ? float.Parse(shape.GetMetadata("bright_t31"),
  System.Globalization.CultureInfo.InvariantCulture) : 0;
                            f.frp = !string.IsNullOrEmpty(shape.GetMetadata("frp")) ? float.Parse(shape.GetMetadata("frp"),
  System.Globalization.CultureInfo.InvariantCulture) : 0;
                            f.daynight = shape.GetMetadata("daynight");
                            f.x = ((ShapePoint)shape).Point.X;
                            f.y = ((ShapePoint)shape).Point.Y;
                            f.date = parsedDate;
                            features.Add(f);
                        }
                    }
                    logInfo("Чтение данных из Shapefile завершился");
                    #endregion
                    #region insert data to db 
                    logInfo("Зпаись данных в базу.....");
                    var connString = ConfigurationSettings.AppSettings["dbConnString"];
                    /*
                    using (SqlConnection connection = new SqlConnection(connString))
                    {
                        connection.Open();
                        //logInfo("conn is open");
                        SqlCommand command = connection.CreateCommand();

                        SqlTransaction transaction;

                        transaction = connection.BeginTransaction("transaction");
                        command.Connection = connection;
                        command.Transaction = transaction;

                        try
                        {
                            if (features != null && features.Count > 0)
                            {
                                command.CommandText = "DELETE FROM dbo.MODIS_C6_GLOBAL_24H";
                                command.ExecuteNonQuery();
                            }
                            for (int i = 0; i < features.Count; i++)
                            {
                                command.Parameters.Clear();
                                command.CommandText = "INSERT INTO dbo.MODIS_C6_GLOBAL_24H (OBJECTID,BRIGHTNESS,SCAN,TRACK,ACQ_DATE,ACQ_TIME,SATELLITE,CONFIDENCE,VERSION,BRIGHT_T31,FRP,DAYNIGHT,Shape) VALUES(@OBJECTID,@BRIGHTNESS,@SCAN,@TRACK,@ACQ_DATE,@ACQ_TIME,@SATELLITE,@CONFIDENCE,@VERSION,@BRIGHT_T31,@FRP,@DAYNIGHT,@Shape)";
                                //command.Parameters.Add("@OBJECTID", SqlDbType.Int).Value = i + 1;
                                //command.Parameters.Add("@BRIGHTNESS", SqlDbType.Float).Value = features[i].brightness;
                                //command.Parameters.Add("@SCAN", SqlDbType.Float).Value = features[i].scan;
                                //command.Parameters.Add("@TRACK", SqlDbType.Float).Value = features[i].track;
                                //command.Parameters.Add("@ACQ_DATE", SqlDbType.DateTime2).Value = features[i].date;
                                //command.Parameters.Add("@ACQ_TIME", SqlDbType.VarChar).Value = features[i].acq_time;
                                //command.Parameters.Add("@SATELLITE", SqlDbType.VarChar).Value = features[i].satellite;
                                //command.Parameters.Add("@CONFIDENCE", SqlDbType.Float).Value = features[i].confidence;
                                //command.Parameters.Add("@VERSION", SqlDbType.VarChar).Value = features[i].version;
                                //command.Parameters.Add("@BRIGHT_T31", SqlDbType.Float).Value = features[i].bright_t31;
                                //command.Parameters.Add("@FRP", SqlDbType.Float).Value = features[i].frp;
                                //command.Parameters.Add("@DAYNIGHT", SqlDbType.VarChar).Value = features[i].daynight;
                                //SqlGeometry point = SqlGeometry.Point(features[i].x, features[i].y, 4326);
                                //command.Parameters.Add(new SqlParameter("@Shape", SqlDbType.Udt));
                                //command.Parameters["@Shape"].UdtTypeName = "geometry";
                                //command.Parameters["@Shape"].Value = point;
                                command.ExecuteNonQuery();
                            }
                            // Attempt to commit the transaction.
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            logInfo(string.Format("ex ", ex.Message));
                            // Attempt to roll back the transaction. 
                            try
                            {
                                transaction.Rollback();
                                logInfo("transaction Rollback");
                            }
                            catch (Exception ex2)
                            {
                                logInfo(string.Format("ex2 ", ex2.Message));
                                throw ex2;
                            }
                        }
                    }
                    */
                    logInfo("Загрузка данных в базу завершилась");

                    #endregion
                    #region removing downloaded shapefiles
                    System.IO.DirectoryInfo di = new DirectoryInfo(extractPath);
                    foreach (FileInfo file in di.GetFiles())
                    {
                        file.Delete();
                    }
                    foreach (DirectoryInfo dir in di.GetDirectories())
                    {
                        dir.Delete(true);
                    }
                    FileInfo fileZip = new FileInfo(downloadPathZip);
                    if (fileZip.Exists)
                    {
                        fileZip.Delete();
                    }
                    #endregion
                    clearLogs();
                    logInfo("Последняя загрузка данных была " + getCurrentTime());
                    var timeToSleep = 15 * 60 * 1000;
                    Thread.Sleep(timeToSleep);
                }
            }
            catch (Exception ex)
            {
                //throw ex;

                _thread.Abort();
                btnStart.Enabled = true;
                btnStop.Enabled = false;
                //logInfo("Ошибка :" + ex.Message);

            }


        }

        private void logInfo(string info)
        {
            if (InvokeRequired)
                Invoke((Action<string>)logInfo, info);
            else
                listBoxStatusess.Items.Insert(0, info);
        }

        private void clearLogs()
        {
            if (InvokeRequired)
                Invoke((Action)clearLogs);
            else
                listBoxStatusess.Items.Clear();
        }


        private string getCurrentTime()
        {
            return System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
        }
    }

    public class Feature
    {
        public double x { get; set; }
        public double y { get; set; }
        public int object_id { get; set; }
        public string satellite { get; set; }
        public float bright_t31 { get; set; }
        public float frp { get; set; }
        public float track { get; set; }
        public float confidence { get; set; }
        public string version { get; set; }
        public float brightness { get; set; }
        public float scan { get; set; }
        public string acq_time { get; set; }
        public string daynight { get; set; }
        public string acq_date { get; set; }
        public DateTime date { get; set; }
    }
}
