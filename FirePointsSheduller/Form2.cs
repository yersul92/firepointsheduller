﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Net;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Diagnostics;
using Catfood.Shapefile;
using Npgsql;
using System.Threading;
//using Microsoft.SqlServer.Types;

namespace FirePointsSheduller
{
    public partial class Form2 : Form
    {
        private string downloadPathZip = System.Windows.Forms.Application.StartupPath + "\\temp_.zip";
        private string extractPath = System.Windows.Forms.Application.StartupPath + "\\temp_";
        private string downloadPathShp = System.Windows.Forms.Application.StartupPath + "\\temp_\\MODIS_C6_Global_24h.shp";
        private string _mainUrl = "http://utility.arcgis.com/usrsvcs/servers/3dba28abdadd4b4a9b39aaa6e79dcb21/rest/services/LiveFeeds/MODIS_Thermal/MapServer/0/query/?f=json&";

        public Form2()
        {
            InitializeComponent();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            _thread.Abort();
            btnStart.Enabled = true;
            btnStop.Enabled = false;
            logInfo(string.Format("Загрузка данных принудительно остановлена в {0}", getCurrentTime()));
        }
        Thread _thread;
        private void btnStart_Click(object sender, EventArgs e)
        {
            _thread = new Thread(new ThreadStart(proccess));
            _thread.Start();
            btnStop.Enabled = true;
            btnStart.Enabled = false;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            _thread = new Thread(new ThreadStart(proccess));
            _thread.Start();
            btnStop.Enabled = true;
            btnStart.Enabled = false;
            Environment.Exit(0);
        }

        private void proccess()
        {
            var connString = ConfigurationSettings.AppSettings["postgreDbConnString"];
            Func<long, DateTime> getDateTimeFromEpoch = (long ticks) =>
            {
                return new DateTime(1970, 1, 1) + TimeSpan.FromSeconds(ticks / 1000);
            };

            Func<string, dynamic> getFeaturesFromNet = (string queryUrl) =>
            {
                try
                {
                    using (WebClient wc = new WebClient())
                    {
                        //var json = wc.DownloadString("http://utility.arcgis.com/usrsvcs/servers/3dba28abdadd4b4a9b39aaa6e79dcb21/rest/services/LiveFeeds/MODIS_Thermal/MapServer/0/query/?f=json&returnGeometry=true&spatialRel=esriSpatialRelIntersects&geometry=%7B%22xmin%22%3A2504688.542848654%2C%22ymin%22%3A2504688.542848654%2C%22xmax%22%3A12523442.714243276%2C%22ymax%22%3A10018754.171394622%2C%22spatialReference%22%3A%7B%22wkid%22%3A102100%7D%7D&geometryType=esriGeometryEnvelope&inSR=102100&outFields=*&outSR=102100&WHERE=" + query);
                        var json = wc.DownloadString(queryUrl);
                        return Newtonsoft.Json.JsonConvert.DeserializeObject(json);
                    }
                }
                catch (Exception ex)
                {
                    logInfo(ex.Message);
                    return null;
                }
                
            };

            Action<bool,string,dynamic> insertFeaturesToDb = (bool firstRun,string postgreConnString, dynamic data) =>
            {
                using (NpgsqlConnection connection = new NpgsqlConnection(postgreConnString))
                {
                    connection.Open();
                    //logInfo("conn is open");
                    NpgsqlCommand command = connection.CreateCommand();

                    NpgsqlTransaction transaction;

                    transaction = connection.BeginTransaction();
                    command.Connection = connection;
                    command.Transaction = transaction;

                    try
                    {
                        if (firstRun)
                        {
                            command.CommandText = "DELETE FROM modis_c6_global_24h";
                            command.ExecuteNonQuery();
                        }
                        for (int i = 0; i < data.features.Count; i++)
                        {
                            command.Parameters.Clear();
                            var dt = getDateTimeFromEpoch((long)data.features[i].attributes.ACQ_DATE);
                            var cmdText = string.Format("INSERT INTO modis_c6_global_24h (gid,brightness,scan,track,acq_date,satellite,confidence,version,bright_t31,frp,daynight,geom,acq_time,created_at) VALUES({0},{1},{2},{3},'{4}','{5}',{6},'{7}',{8},{9},'{10}',{11},'{12}',{13})",
                                data.features[i].attributes.OBJECTID,
                                data.features[i].attributes.BRIGHTNESS.ToString().Replace(",", "."),
                                data.features[i].attributes.SCAN.ToString().Replace(",", "."),
                                data.features[i].attributes.TRACK.ToString().Replace(",", "."),
                                dt.ToString("yyyy-MM-dd HH:mm:ss"),
                                data.features[i].attributes.SATELLITE,
                                data.features[i].attributes.CONFIDENCE.ToString().Replace(",", "."),
                                data.features[i].attributes.VERSION,
                                data.features[i].attributes.BRIGHT_T31.ToString().Replace(",", "."),
                                data.features[i].attributes.FRP.ToString().Replace(",", "."),
                                data.features[i].attributes.DAYNIGHT,
                                string.Format("st_geomfromtext('Point({0} {1})',3857)", data.features[i].geometry.x.ToString().Replace(",", "."), data.features[i].geometry.y.ToString().Replace(",", ".")),
                                dt.ToString("HH:mm:ss"),
                                string.Format("'{0}'", dt.ToString("yyyy-MM-dd HH:mm:ss"))
                                );
                            #region musor
                            /*
                            command.CommandText = "INSERT INTO modis_c6_global_24h (gid,brightness,scan,track,acq_date,satellite,confidence,version,bright_t31,frp,daynight,geom) VALUES(@OBJECTID,@BRIGHTNESS,@SCAN,@TRACK,@ACQ_DATE,@SATELLITE,@CONFIDENCE,@VERSION,@BRIGHT_T31,@FRP,@DAYNIGHT,@Shape)";
                            command.Parameters.Add("@OBJECTID", NpgsqlTypes.NpgsqlDbType.Integer).Value = data.features[i].attributes.OBJECTID;
                            command.Parameters.Add("@BRIGHTNESS", NpgsqlTypes.NpgsqlDbType.Double).Value = data.features[i].attributes.BRIGHTNESS;
                            command.Parameters.Add("@SCAN", NpgsqlTypes.NpgsqlDbType.Double).Value = data.features[i].attributes.SCAN;
                            command.Parameters.Add("@TRACK", NpgsqlTypes.NpgsqlDbType.Double).Value = data.features[i].attributes.TRACK;
                            command.Parameters.Add("@ACQ_DATE", NpgsqlTypes.NpgsqlDbType.Timestamp).Value = getDateTimeFromEpoch((long)data.features[i].attributes.ACQ_DATE);
                            //command.Parameters.Add("@ACQ_TIME", NpgsqlTypes.NpgsqlDbType.Varchar).Value = data.features[i].attributes.ACQ_TIME;
                            command.Parameters.Add("@SATELLITE", NpgsqlTypes.NpgsqlDbType.Varchar).Value = data.features[i].attributes.SATELLITE;
                            command.Parameters.Add("@CONFIDENCE", NpgsqlTypes.NpgsqlDbType.Double).Value = data.features[i].attributes.CONFIDENCE;
                            command.Parameters.Add("@VERSION", NpgsqlTypes.NpgsqlDbType.Varchar).Value = data.features[i].attributes.VERSION;
                            command.Parameters.Add("@BRIGHT_T31", NpgsqlTypes.NpgsqlDbType.Double).Value = data.features[i].attributes.BRIGHT_T31;
                            command.Parameters.Add("@FRP", NpgsqlTypes.NpgsqlDbType.Double).Value = data.features[i].attributes.FRP;
                            command.Parameters.Add("@DAYNIGHT", NpgsqlTypes.NpgsqlDbType.Varchar).Value = data.features[i].attributes.DAYNIGHT;
                            var point = new NpgsqlTypes.NpgsqlPoint((double)data.features[i].geometry.x, (double)data.features[i].geometry.y);
                            command.Parameters.Add("@Shape", NpgsqlTypes.NpgsqlDbType.Geometry).Value = point;
                            //command.Parameters.AddWithValue("@Shape", string.Format("st_geomfromtext('Point({0} {1}',3857)", data.features[i].geometry.x.ToString().Replace(",","."), data.features[i].geometry.y.ToString().Replace(",", ".")));
                            */
                            #endregion
                            command.CommandText = cmdText;
                            command.ExecuteNonQuery();
                        }
                        command.Parameters.Clear();
                        command.CommandText = "delete from modis_c6_global_24h where gid not in (select f.gid from modis_c6_global_24h f inner join country_polygon c on st_intersects(f.geom,c.geom))";
                        command.ExecuteNonQuery();
                        command.Parameters.Clear();
                        command.CommandText = "UPDATE modis_c6_global_24h as f SET region_id = d.region_id, district_id = d.gid FROM district_polygon as d WHERE st_intersects(f.geom, d.geom)";
                        command.ExecuteNonQuery();
                        // Attempt to commit the transaction.
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        logInfo(string.Format("ex ", ex.Message));
                        // Attempt to roll back the transaction. 
                        try
                        {
                            transaction.Rollback();
                            logInfo("transaction Rollback");
                        }
                        catch (Exception ex2)
                        {
                            logInfo(string.Format("ex2 ", ex2.Message));
                            throw ex2;
                        }
                    }
                }
            };

            Func<dynamic, List<int>> getObjIdsIntArray = (dynamic data) => {
                var res = new List<int>();
                if (data["objectIds"] != null && data["objectIds"].Count > 0)
                {
                    for (int i = 0; i < data["objectIds"].Count; i++)
                    {
                        res.Add((int)data["objectIds"][i]);
                    }
                }
                return res;                      
            };

            try
            {
                //var logInfo = new Action<string>((string message) => { listBoxStatusess.Items.Insert(0, message); });
                while (true)
                {
                    logInfo("Процесс начался...");
                    var hours_24_later = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,0,0,0).AddHours(-6);
                    string queryWhereClause = string.Format("ACQ_DATE>=timestamp '{0}'", hours_24_later.ToString("yyyy-MM-dd HH:mm:ss"));
                    var getObjIdsUrl = string.Format("{0}WHERE={1}&returnIdsOnly=true",_mainUrl, queryWhereClause);
                    logInfo(string.Format("Загрузка всех ObjectId после {0} ...", hours_24_later.ToString("yyyy-MM-dd HH:mm:ss")));
                    var objectIds = getFeaturesFromNet(getObjIdsUrl);
                    logInfo(string.Format("Завершение загрузки всех ObjectId после {0}", hours_24_later.ToString("yyyy-MM-dd HH:mm:ss")));
                    logInfo(string.Format("Загрузка атрибутивных данных по всем ObjectId после {0} ...", hours_24_later.ToString("yyyy-MM-dd HH:mm:ss")));
                    if (objectIds!=null&&objectIds["objectIds"] != null && objectIds["objectIds"].Count > 0) {
                        var objIdsIntArr = getObjIdsIntArray(objectIds);
                        var totalCount = (int)objectIds["objectIds"].Count;
                        int quotient = totalCount / 1000;
                        var d = objectIds["objectIds"];

                        int remainder = totalCount % 1000;
                        for (int i = 0; i < quotient; i++) {
                            int indexAt = i*1000;
                            var sliced = objIdsIntArr.GetRange(indexAt, 1000).ToArray();
                            var getAttrsUrl = string.Format("{0}WHERE={1}&objectIds={2}&outFields=*", _mainUrl, queryWhereClause, string.Join(",",sliced));
                            var attrs = getFeaturesFromNet(getAttrsUrl);
                            if (attrs != null)
                            {
                                insertFeaturesToDb(i == 0, connString, attrs);
                            }
                        }
                        if (remainder > 0) {
                            var slicedRemainder = objIdsIntArr.GetRange(quotient*1000, remainder).ToArray();
                            var getAttrsUrl = string.Format("{0}WHERE={1}&objectIds={2}&outFields=*", _mainUrl, queryWhereClause, string.Join(",", slicedRemainder));
                            var attrs = getFeaturesFromNet(getAttrsUrl);
                            if (attrs != null)
                            {
                                insertFeaturesToDb(false, connString, attrs);
                            }
                        }
                    }
                    logInfo(string.Format("Завершение загрузки атрибутивных данных по всем ObjectId после {0}", hours_24_later.ToString("yyyy-MM-dd HH:mm:ss")));
                    clearLogs();
                    logInfo("Последняя загрузка данных была " + getCurrentTime());
                    var timeToSleep = 4 * 60 * 1000;
                    Thread.Sleep(timeToSleep);
                }
            }
            catch (Exception ex)
            {
                _thread.Abort();
                btnStart.Enabled = true;
                btnStop.Enabled = false;
                logInfo("Ошибка :" + ex.Message);
            }
        }

        private void logInfo(string info)
        {
            if (InvokeRequired)
                Invoke((Action<string>)logInfo, info);
            else
                listBoxStatusess.Items.Insert(0, info);
        }

        private void clearLogs()
        {
            if (InvokeRequired)
                Invoke((Action)clearLogs);
            else
                listBoxStatusess.Items.Clear();
        }


        private string getCurrentTime()
        {
            return System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
        }
    }

}
